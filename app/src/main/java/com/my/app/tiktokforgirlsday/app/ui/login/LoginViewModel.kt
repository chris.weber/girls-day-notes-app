package com.my.app.tiktokforgirlsday.app.ui.login

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.my.app.tiktokforgirlsday.app.navigation.NavRoute
import com.my.app.tiktokforgirlsday.app.navigation.navigateTo
import com.my.app.tiktokforgirlsday.aufgaben.Login
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.launch

class LoginViewModel: ViewModel() {

    private val _uiState = MutableStateFlow<LceState>(LceState.Loading)

    init {
        viewModelScope.launch {
            delay(2000)
            _uiState.emit(LceState.Content)
        }
    }

    fun onLoginClicked(username: String, password: String) {
        val loginSuccess = Login.istLoginErfolgreich(username, password)

        if(loginSuccess){
            NavRoute.TODO_LIST.navigateTo()
        }
    }

    sealed interface LceState {
        object Loading: LceState
        object Content: LceState
    }
}
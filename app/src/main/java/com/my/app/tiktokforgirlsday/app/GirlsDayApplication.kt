package com.my.app.tiktokforgirlsday.app

import android.app.Application
import android.content.Context
import androidx.datastore.preferences.preferencesDataStore
import com.my.app.tiktokforgirlsday.app.di.DI

class GirlsDayApplication: Application() {
    override fun onCreate() {
        super.onCreate()
        DI.persistence = applicationContext.dataStore//applicationContext.getSharedPreferences("GIRLS_DAY", MODE_PRIVATE)
    }
}

private val Context.dataStore by preferencesDataStore(
    name = "GIRLS_DAY_DS"
)
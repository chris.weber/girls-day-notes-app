package com.my.app.tiktokforgirlsday.app.di

import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.Preferences
import com.my.app.tiktokforgirlsday.app.repository.AufgabenRepository

data object DI {
    lateinit var persistence: DataStore<Preferences>

    val aufgabenRepository by lazy {
        AufgabenRepository()
    }
}
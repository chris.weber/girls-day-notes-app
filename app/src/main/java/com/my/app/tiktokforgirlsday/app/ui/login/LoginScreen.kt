package com.my.app.tiktokforgirlsday.app.ui.login

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.height
import androidx.compose.material3.Button
import androidx.compose.material3.Text
import androidx.compose.material3.TextField
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.my.app.tiktokforgirlsday.app.ui.theme.GirlsDayTheme

@Composable
fun LoginScreen(
    viewModel: LoginViewModel = LoginViewModel()
) {
    LoginScreenContent(
        onLoginClicked = { user, pass ->
            viewModel.onLoginClicked(user, pass)
        }
    )
}

@Composable
fun LoginScreenContent(
    onLoginClicked: (String, String) -> Unit,
) {
    Column(
        modifier = Modifier.fillMaxSize(),
        verticalArrangement = Arrangement.Center,
        horizontalAlignment = Alignment.CenterHorizontally
    ){
        var user by remember { mutableStateOf("GirlsDay") }
        var password by remember { mutableStateOf("lernen") }

        Text(text = "Nutzer")
        TextField(value = user, onValueChange = { user = it} )
        Spacer(modifier = Modifier.height(16.dp))
        Text(text = "Passwort")
        TextField(value = password, onValueChange = { password = it} )
        Spacer(modifier = Modifier.height(32.dp))
        Button(
            onClick = { onLoginClicked(user, password) }
        ) {
            Text(text = "Login")
        }
    }

}


@Preview(showBackground = true)
@Composable
private fun LoginScreenContentPreview() {
    GirlsDayTheme {
        LoginScreenContent(
            onLoginClicked = {_,_ -> }
        )
    }
}
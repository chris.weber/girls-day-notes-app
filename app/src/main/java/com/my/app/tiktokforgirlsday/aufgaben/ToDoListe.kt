package com.my.app.tiktokforgirlsday.aufgaben

import androidx.compose.runtime.Composable
import androidx.compose.ui.tooling.preview.Preview
import com.my.app.tiktokforgirlsday.app.data.Aufgabe
import com.my.app.tiktokforgirlsday.app.ui.theme.GirlsDayTheme
import com.my.app.tiktokforgirlsday.dsl.AufgabenListe
import com.my.app.tiktokforgirlsday.dsl.falsch
import com.my.app.tiktokforgirlsday.dsl.wahr

object ToDoListe {

    @Composable
    fun ZeichneEineEinzelneAufgabe(
        aufgabe: Aufgabe
    ) {
        //TODO
    }

    @Composable
    fun ZeichneDieAufgabenListe(
        aufgaben: AufgabenListe
    ) {
        //TODO
    }
}

@Preview(showBackground = true)
@Composable
private fun EinelAufgabeVorschau() {
    GirlsDayTheme {
        ToDoListe.ZeichneEineEinzelneAufgabe(
            aufgabe = Aufgabe(
                istErledigt = falsch,
                text = "Hausaufgaben machen"
            )
        )
    }
}

@Preview(showBackground = true, showSystemUi = true)
@Composable
private fun AufgabenListeVorschau() {
    GirlsDayTheme {
        ToDoListe.ZeichneDieAufgabenListe(
            aufgaben = listOf(
                Aufgabe(wahr, "Hausaufgaben machen"),
                Aufgabe(falsch, "Computer spielen"),
                Aufgabe(falsch, "Mit dem Hund gassi gehen"),
            )
        )
    }
}
package com.my.app.tiktokforgirlsday.app.repository

import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.Preferences
import androidx.datastore.preferences.core.booleanPreferencesKey
import androidx.datastore.preferences.core.edit
import androidx.datastore.preferences.core.emptyPreferences
import com.my.app.tiktokforgirlsday.app.data.Aufgabe
import com.my.app.tiktokforgirlsday.app.di.DI
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.map
import java.io.IOException

class AufgabenRepository(
    private val persistence: DataStore<Preferences> = DI.persistence,
) {
//
//    init {
//        CoroutineScope(Dispatchers.IO).launch {
//            EXAMPLE_LIST.forEach {
//                delay(1000)
//                addAufgabe(it)
//            }
//        }
//    }

    fun getAllAufgaben(): Flow<List<Aufgabe>> {
        return persistence.data
            .catch { exception ->
                // dataStore.data throws an IOException when an error is encountered when reading data
                if (exception is IOException) {
                    emit(emptyPreferences())
                } else {
                    throw exception
                }
            }.map {
                it.asMap()
                    .filterKeys { it.name.startsWith(KEY_AUFGABEN) }
                    .map { (key, value) ->
                        (value as? Boolean)?.let { Aufgabe(value, key.name.removePrefix("${KEY_AUFGABEN}__")) }
                    }
                        .filterNotNull()
            }
    }

    suspend fun addAufgabe(aufgabe: Aufgabe) {
        persistence.edit {
            it.set(buildPrefKey(aufgabe.text), aufgabe.istErledigt)
        }
    }

    suspend fun updateAufgabe(aufgabe: Aufgabe) {
        persistence.edit {
            it.set(buildPrefKey(aufgabe.text), aufgabe.istErledigt)
        }
    }

    suspend fun removeAufgabe(aufgabe: Aufgabe) {
        persistence.edit {
            it.remove(buildPrefKey(aufgabe.text))
        }
    }

    fun buildPrefKey(text: String) = booleanPreferencesKey(
        "${KEY_AUFGABEN}__$text"
    )

    companion object {
        private val KEY_AUFGABEN = "KEY_AUFGABEN"
    }
}
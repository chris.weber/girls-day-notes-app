package com.my.app.tiktokforgirlsday.aufgaben


import com.my.app.tiktokforgirlsday.dsl.WahrFalsch
import com.my.app.tiktokforgirlsday.dsl.Wort
import com.my.app.tiktokforgirlsday.dsl.falsch
import com.my.app.tiktokforgirlsday.dsl.istGleich
import com.my.app.tiktokforgirlsday.dsl.wahr
import com.my.app.tiktokforgirlsday.dsl.wenn

object Login {
    fun prüfeBenutzer(benutzerName: Wort): WahrFalsch {
        // Beispiel
        val benutzerNameRichtig = benutzerName istGleich "GirlsDay"
        wenn(benutzerNameRichtig) {
            return wahr
        } ansonsten {
            // Mach gar nichts
        }
        return falsch
    }

    fun prüfePasswort(passwort: Wort): WahrFalsch {
        // TODO
        return true
    }

    fun istLoginErfolgreich(benutzerName: Wort, passwort: Wort): WahrFalsch {
        // TODO
        val istBenutzerEingeloggt = prüfeBenutzer(benutzerName)
        return istBenutzerEingeloggt
    }

}



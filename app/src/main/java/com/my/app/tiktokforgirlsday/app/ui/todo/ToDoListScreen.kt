package com.my.app.tiktokforgirlsday.app.ui.todo

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Create
import androidx.compose.material.icons.filled.Delete
import androidx.compose.material3.Card
import androidx.compose.material3.Checkbox
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.FloatingActionButton
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.LocalTextStyle
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Text
import androidx.compose.material3.TopAppBar
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.style.TextDecoration
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.my.app.tiktokforgirlsday.app.data.Aufgabe
import com.my.app.tiktokforgirlsday.app.navigation.NavRoute
import com.my.app.tiktokforgirlsday.app.navigation.navigateTo
import com.my.app.tiktokforgirlsday.app.ui.theme.GirlsDayTheme
import com.my.app.tiktokforgirlsday.app.ui.util.LoadingScreen
import com.my.app.tiktokforgirlsday.aufgaben.ToDoListe

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun ToDoListScreen(
    viewModel: ToDoListViewModel = ToDoListViewModel(),
) {
    Scaffold(
        topBar = {
            TopAppBar(
                title = {
                    Text(text = "ToDo Liste")
                }
            )
        },
        floatingActionButton = {
            FloatingActionButton(
                onClick = {
                    NavRoute.TODO_LIST_ADD.navigateTo()
                }
            ) {
                Icon(imageVector = Icons.Default.Create, contentDescription = null)
            }
        }
    ) {

        when (val state = viewModel.uiState.collectAsState(ToDoListViewModel.LceState.Loading).value) {
            is ToDoListViewModel.LceState.Content -> {
                ToDoScreenContent(
                    state = state,
                    onUpdateItem = { aufgabe ->
                        viewModel.updateAufgabe(aufgabe)
                    },
                    onDeleteItem = { aufgabe ->
                        viewModel.deleteAufgabe(aufgabe)
                    },
                    contentPadding = it,
                )
            }

            else -> LoadingScreen()
        }
    }
}

@Composable
fun ToDoScreenContent(
    state: ToDoListViewModel.LceState.Content,
    onUpdateItem: (aufgabe: Aufgabe) -> Unit,
    onDeleteItem: (aufgabe: Aufgabe) -> Unit,
    contentPadding: PaddingValues
) {
    Column(
        modifier = Modifier.padding(contentPadding)
    ) {
        ToDoParameterHack.onCheckChange = onUpdateItem
        ToDoParameterHack.onDeleteClick = onDeleteItem
        ToDoListe.ZeichneDieAufgabenListe(aufgaben = state.todos)
    }
//    LazyColumn(
//        contentPadding = contentPadding
//    ) {
//        items(state.todos) { todo: Aufgabe ->
//            ToDoListItem(
//                todo = todo,
//                onUpdateItem = {
//                    onUpdateItem(it)
//                },
//                onDeleteItem = {
//                    onDeleteItem(todo)
//                }
//            )
//        }
//    }
}

@Composable
fun ToDoListItem(
    todo: Aufgabe,
    onUpdateItem: (aufgabe: Aufgabe) -> Unit,
    onDeleteItem: () -> Unit,
) {
    Card(
        modifier = Modifier.padding(vertical = 8.dp, horizontal = 16.dp)
    ) {
        Row(
            modifier = Modifier
                .fillMaxWidth()
                .padding(8.dp)
        ) {
            Checkbox(
                checked = todo.istErledigt,
                onCheckedChange = {
                    onUpdateItem(todo.copy(istErledigt = it))
                }
            )
            Text(
                text = todo.text,
                style = if(todo.istErledigt) {
                    LocalTextStyle.current.copy(textDecoration = TextDecoration.LineThrough)
                } else {
                    LocalTextStyle.current
                },
                modifier = Modifier
                    .weight(1F)
                    .padding(vertical = 12.dp),
            )
            IconButton(
                onClick = onDeleteItem,
                modifier = Modifier.align(Alignment.Bottom)
            ) {
                Icon(
                    imageVector = Icons.Default.Delete,
                    contentDescription = null
                )
            }
        }
    }
}

@Preview(showBackground = true, showSystemUi = true)
@Composable
private fun ToDoListPreview() {
    GirlsDayTheme {
        ToDoScreenContent(
            state = ToDoListViewModel.LceState.Content(
                todos = listOf(
                    Aufgabe(false, "Needs to be done"),
                    Aufgabe(true, "done"),
                    Aufgabe(true, "and another done one"),
                    Aufgabe(false, "and another undone one that contains a lot longer text that goes on and on and on and on and on and on and on and on and on"),
                )
            ),
            onUpdateItem = { },
            onDeleteItem = { },
            contentPadding = PaddingValues()
        )
    }
}
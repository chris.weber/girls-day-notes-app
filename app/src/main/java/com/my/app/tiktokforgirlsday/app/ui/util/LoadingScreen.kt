package com.my.app.tiktokforgirlsday.app.ui.util

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.my.app.tiktokforgirlsday.app.ui.theme.GirlsDayTheme

@Composable
fun LoadingScreen() {
    Column(
        modifier = Modifier.fillMaxSize(),
        verticalArrangement = Arrangement.Center,
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        val boxShape = RoundedCornerShape(16.dp)
        Box (
            contentAlignment = Alignment.Center,
            modifier = Modifier
                .background(
                    color = MaterialTheme.colorScheme.onSurface.copy(alpha = 0.2F),
                    shape = boxShape,
                )
                .padding(32.dp)
        ){
            CircularProgressIndicator(
                modifier = Modifier.size(80.dp)
            )
            Text(text = "Loading")
        }
    }
}

@Preview(showBackground = true)
@Composable
private fun LoadingScreenPreview() {
    GirlsDayTheme {
        LoadingScreen()
    }
}
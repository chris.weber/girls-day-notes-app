package com.my.app.tiktokforgirlsday.app.data

data class Aufgabe(
    val istErledigt: Boolean,
    val text: String,
)

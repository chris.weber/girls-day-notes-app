package com.my.app.tiktokforgirlsday.dsl

import com.my.app.tiktokforgirlsday.app.data.Aufgabe

// Types

//Boolean
typealias WahrFalsch = Boolean

val wahr = true
val falsch = false

infix fun WahrFalsch.und(andereBedingung: WahrFalsch): WahrFalsch {
    return this && andereBedingung
}

// String
typealias Wort = String

// List
typealias AufgabenListe = List<Aufgabe>


// Functions

inline fun wenn(bedingung: WahrFalsch, wennWahr: () -> Unit): BooleanWithElse {
    if(bedingung) {
        wennWahr()
    }
    return BooleanWithElse(bedingung)
}

class BooleanWithElse(val bedingung: WahrFalsch) {
    inline infix fun ansonsten(wennFalsch: () -> Unit) {
        if (bedingung) return
        wennFalsch()
    }
}

infix fun Any?.istGleich(anderes: Any?): WahrFalsch {
    return this == anderes
}

inline fun <T> Iterable<T>.fürJedeAufgabe(action: (T) -> Unit): Unit {
    this.forEach(action)
}


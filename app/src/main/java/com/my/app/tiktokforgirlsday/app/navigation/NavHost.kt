package com.my.app.tiktokforgirlsday.app.navigation

import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import kotlinx.coroutines.flow.MutableStateFlow

object NavHost {
    private val currentPosition: MutableStateFlow<NavRoute> = MutableStateFlow(NavRoute.DEFAULT)

    @Composable
    fun ShowContent() {
        currentPosition.collectAsState().value.let {
            it.onNavigateTo()
        }
    }

    fun navigateTo(navRoute: NavRoute) {
        currentPosition.tryEmit(navRoute)
    }
}

package com.my.app.tiktokforgirlsday.app.navigation

import androidx.compose.runtime.Composable
import com.my.app.tiktokforgirlsday.app.ui.login.LoginScreen
import com.my.app.tiktokforgirlsday.app.ui.todo.AddToDoScreen
import com.my.app.tiktokforgirlsday.app.ui.todo.ToDoListScreen

enum class NavRoute(
    val onNavigateTo: @Composable () -> Unit,
) {
    DEFAULT ( onNavigateTo = { LoginScreen() } ),
    TODO_LIST( onNavigateTo = { ToDoListScreen() }),
    TODO_LIST_ADD( onNavigateTo = { AddToDoScreen() }),
}

fun NavRoute.navigateTo() {
    NavHost.navigateTo(this)
}
package com.my.app.tiktokforgirlsday.app.ui.todo

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Close
import androidx.compose.material.icons.filled.Done
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.FloatingActionButton
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Text
import androidx.compose.material3.TextField
import androidx.compose.material3.TopAppBar
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import com.my.app.tiktokforgirlsday.app.data.Aufgabe
import com.my.app.tiktokforgirlsday.app.navigation.NavRoute
import com.my.app.tiktokforgirlsday.app.navigation.navigateTo

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun AddToDoScreen(
    viewModel: ToDoListViewModel = ToDoListViewModel(),
) {
    var text by remember {
        mutableStateOf("")
    }
    Scaffold(
        topBar = {
            TopAppBar(
                title = {
                    Text(text = "Neue Aufgabe")
                },
                actions = {
                    IconButton(
                        onClick = { NavRoute.TODO_LIST.navigateTo() }
                    ) {
                        Icon(imageVector = Icons.Default.Close, contentDescription = null)
                    }
                }
            )
        },
        floatingActionButton = {
            FloatingActionButton(
                onClick = {
                    viewModel.addAufgabe(
                        Aufgabe(
                            false,
                            text
                        )
                    )
                    NavRoute.TODO_LIST.navigateTo()
                }
            ) {
                Icon(imageVector = Icons.Default.Done, contentDescription = null)
            }
        }
    ) {
        //TODO
        Column(
            modifier = Modifier.padding(it)
        ){

            TextField(
                value = text,
                onValueChange = {text = it},
                modifier = Modifier.fillMaxWidth()
                    .padding(32.dp)
            )
        }
    }
}

package com.my.app.tiktokforgirlsday.app.ui.todo

import android.util.Log
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.my.app.tiktokforgirlsday.app.data.Aufgabe
import com.my.app.tiktokforgirlsday.app.di.DI
import com.my.app.tiktokforgirlsday.app.repository.AufgabenRepository
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.launch

class ToDoListViewModel(
    private val todoRepository: AufgabenRepository = DI.aufgabenRepository,
): ViewModel() {
    private val errorHandler = CoroutineExceptionHandler { coroutineContext, throwable ->
        Log.e(TAG, "Error", throwable)
    }
    val uiState = todoRepository.getAllAufgaben().map {
        LceState.Content(it)
    }//.stateIn(viewModelScope, SharingStarted.Eagerly, LceState.Loading)

    fun addAufgabe(aufgabe: Aufgabe) {
        viewModelScope.launch(errorHandler) {
            todoRepository.addAufgabe(aufgabe)
        }
    }

    fun updateAufgabe(aufgabe: Aufgabe) {
        viewModelScope.launch(errorHandler) {
            todoRepository.updateAufgabe(aufgabe)
        }
    }

    fun deleteAufgabe(aufgabe: Aufgabe) {
        viewModelScope.launch(errorHandler) {
            todoRepository.removeAufgabe(aufgabe)
        }
    }

    companion object {
        private const val TAG = "ToDoList"
    }

    sealed interface LceState {
        object Loading: LceState
        data class Content(
            val todos: List<Aufgabe>,
        ) : LceState
    }
}
package com.my.app.tiktokforgirlsday.dsl

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.ColumnScope
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.RowScope
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Delete
import androidx.compose.material3.Card
import androidx.compose.material3.CardDefaults
import androidx.compose.material3.Checkbox
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.dp
import com.my.app.tiktokforgirlsday.app.data.Aufgabe
import com.my.app.tiktokforgirlsday.app.ui.todo.ToDoParameterHack

@Composable
fun Reihe(
    hintergrundFarbe: Color? = null,
    content: @Composable RowScope.() -> Unit
) {
    val baseModifier = hintergrundFarbe?.let {
        Modifier.background(color = hintergrundFarbe)
    } ?: Modifier
    Row(
        modifier = baseModifier.fillMaxWidth(),
        horizontalArrangement = Arrangement.SpaceBetween,
        verticalAlignment = Alignment.CenterVertically,
        content = content,
    )
}

@Composable
fun Spalte(
    hintergrundFarbe: Color? = null,
    starteVorne: WahrFalsch = falsch,
    content: @Composable ColumnScope.() -> Unit
) {
    val baseModifier = hintergrundFarbe?.let {
        Modifier.background(color = hintergrundFarbe)
    } ?: Modifier
    Column(
        modifier = baseModifier,
        horizontalAlignment = if (starteVorne)
            Alignment.Start
        else
            Alignment.CenterHorizontally,
        verticalArrangement = Arrangement.Center,
        content = content,
    )
}

@Composable
fun Karte(
    hintergrundFarbe: Color? = null,
    content: @Composable ColumnScope.() -> Unit,
) {
    val farben = if (hintergrundFarbe != null) {
        CardDefaults.cardColors(containerColor = hintergrundFarbe)
    } else CardDefaults.cardColors()
    Card(
        colors = farben,
        modifier = Modifier
            .fillMaxWidth()
            .padding(vertical = 8.dp, horizontal = 16.dp),
        content = content,
    )
}

@Composable
fun ErledigtBox(aufgabe: Aufgabe) {
    Checkbox(
        checked = aufgabe.istErledigt,
        onCheckedChange = { ToDoParameterHack.onCheckChange(aufgabe.copy(istErledigt = it)) }
    )
}

@Composable
fun LöschenKnopf(aufgabe: Aufgabe) {
    IconButton(onClick = { ToDoParameterHack.onDeleteClick(aufgabe) }) {
        Icon(imageVector = Icons.Default.Delete, contentDescription = null)
    }
}
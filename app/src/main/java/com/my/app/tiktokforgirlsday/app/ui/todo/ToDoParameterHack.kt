package com.my.app.tiktokforgirlsday.app.ui.todo

import com.my.app.tiktokforgirlsday.app.data.Aufgabe

object ToDoParameterHack {
    val onCheckChangedMap = mutableMapOf<String, CheckChangedCallback>()

    lateinit var onCheckChange: (aufgabe: Aufgabe) -> Unit
    lateinit var onDeleteClick: (aufgabe: Aufgabe) -> Unit
}

typealias CheckChangedCallback = (Boolean) -> Unit